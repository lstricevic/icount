# icount #

Icount is a small utility for counting the number of CPU instructions which were executed while running an executable binary. It works by running the executable step by step (instruction by instruction) using `ptrace()` system call and counting the steps along the way. This can be very slow, but it does not require any kind of code instrumentation.

Current version works on Linux, although it can be easily converted to any other POSIX system.

###Compilation###

		make

###Usage###
		icount <bin_executable> [arguments] ...

