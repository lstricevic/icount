/*
icount v0.1
Counts instructions executed during program run
Lazar Stricevic, 2017
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ptrace.h>



int main(int argc, char **argv)
{
    if(argc == 1)
    {
        fprintf(stderr, "usage: icount <executable_file> [arguments] \n");
        exit(-1);
    }

    char **callargv=&(argv[1]);
    const char *callpath = argv[1];
    pid_t pid;

    switch(pid = fork())
    {
        case -1: /* fork error*/
        {
            perror("fork()");
            exit(-1);
        }   

        case 0: /* child process */
        {
            ptrace(PTRACE_TRACEME, NULL, NULL);     /* allow child process to be traced */
            execv(callpath, callargv);                /* child will be stopped here */
            perror("execv()");
            exit(-1);
        }
        
        /* parent continues execution */
    }


    int status;

    long long icounter=0;


    wait(&status);

    while(1)
    {       
        if(WIFEXITED(status) || (WIFSIGNALED(status) && WTERMSIG(status) == SIGKILL))
        {
            printf("process %d terminated, instructions %lld \n", pid, icounter);
            exit(0);
        }
#if(DEBUG)
#include <sys/user.h>
#include <sys/reg.h>
        if(WIFSTOPPED(status))
        {           
            long rip =  ptrace(PTRACE_PEEKUSER, pid, 8 * RIP, NULL)-1;
                printf("process %d stopped at 0x%lx\n", pid, rip);
        }
#endif
                /* next instruction */
		if(
	                ptrace(PTRACE_SINGLESTEP, pid, NULL, NULL)
		){
		/* on some architectures (e.g. arm, mips, ...)  PTRACE_SINGLESTEP is not supported*/
			perror("ptrace()");
			exit(-1);
		}

		icounter++;
                wait(&status);


    }
}

