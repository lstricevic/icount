
#### src = $(wildcard *.c)
src = icount.c
obj = $(src:.c=.o)
CFLAGS = -Wall

icount: $(src)
	$(CC) $(CFLAGS) -o $@ $^

.PHONY: clean
clean:
	rm -f $(obj) icount


